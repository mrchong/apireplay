var express = require('express');
var cors = require('cors');
var _ = require('lodash');
const fs = require('fs');
const https = require('https');
const key = fs.readFileSync('./server.key');
const cert = fs.readFileSync('./server.cert');
var app = express();

var mockData = require('./mock2.json');
var responses = {};
_.forEach(mockData, function(val, key) {
    // console.log('valKey: ', key);
    const url = _.get(val, 'request.url');
    if (url) {
        var parts = url.split('/');
        parts.shift();
        parts.shift();
        parts.shift();
        
        responses['/' + parts.join('/')] = {
            ..._.get(val, 'response', null),
            responseMetaData: _.get(val, 'responseMetaData', null)
        };
    }
});

app.use(cors());

function setResponse(req, res, next) {
    const respData = responses[req.url] || responses[req.url + '/'] || responses[req.url.substring(0, req.url.length-1)];
    console.log('request: ', req.url);
    var headers = _.get(respData, 'responseMetaData.headers', {});
    _.forEach(headers, (val, key) => {
        if (key !== 'set-cookie') {
         //   res.set(key, val);
        }
    });
    console.log('url: ', req.url);
    res.set('Content-Type', _.get(respData, 'responseMetaData.headers.content-type', 'text/html'));
    const body = _.get(respData, 'body', '');
    if (!body) {
        res.se
    }
    res.status(body ? 200: 404).send(body);
}

app.get('/*', function (req, res, next) {
  //res.json({msg: 'This is CORS-enabled for all origins!'});
  setResponse(req, res, next);
});
app.post('/*', function (req, res, next) {
  setResponse(req, res, next);
});
app.put('/*', function (req, res, next) {
    setResponse(req, res, next);
});
app.delete('/*', function (req, res, next) {
    setResponse(req, res, next);
});
/*
app.listen(80, function () {
  console.log('CORS-enabled web server listening on port 80')
})*/

const server = https.createServer({key: key, cert: cert }, app);
server.listen(3001, () => { console.log('listening on 80') });